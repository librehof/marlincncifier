# marlincncifier

GPLv3 (C) 2021 [librehof.com](http://librehof.com)

- Prepare custom **[Marlin 2 firmware](https://marlinfw.org)** for your CNC
- Preferably use a board with **Trinamic** stepper drivers 
  - If you are lucky you might be able to configure a good StallGuard threshold, so your machine will **stop before breaking**  
- Marlin's **real-time** position function will be enabled, so you can use together with: 
  - **[MarlinDroid](http://librehof.com)** and **[bCNC](https://github.com/vlachoudis/bCNC)** 
- [Legal notice](NOTICE)


## Instructions

- Make sure you run **linux**, or possibly [git for windows](https://gitforwindows.org)
- **run:** `./setup --help`
- **run:** `./setup <your-short-machine-name>`
  - The default config uses the **LPC1769** board
  - A customized pinfile example for LPC1768/9 is available:  
    `pinmods/Marlin-src-pins-lpc1768-pins_BTT_SKR_V1_4.h`
- **follow instructions**
- **run:** `./setup <machine> 2.0.6.1` 
- **follow instructions**
- The actual Firmware build has to be done separately (with vscode)


## Calibration

Once your machine is up and running with the new firmware, then:
- measure the motion error in XYZ
- adjust **STEPS_PER_UNIT** in `config/<machine>.ini`
- re-setup, rebuild and install firmware again
- your config and resulting firmware will be unique for your machine ! 
- make a **safe copy** of generated backup files and firmware.bin when calibration is done


## Commands

### ./setup

```
Setup a specific version of Marlin 2 for CNC use

Usage (initial setup for configuration)
 ./setup <short-machine-name> [customscript]

Generated files to be reviewed/modified:
 config/<machine>.ini # includes ref to board and pinfile
 config/<machine>.sh  # if customscript

Usage (optionally use custom pinfile once configured)
 ./setup <machine> <marlin-ver> custompins [<altsource>]

Copied pinfile for customization:
 config/<machine>-pins.h

Usage (make marlin mod):
 ./setup <machine> <marlin-ver> [clean|noclean]

Verified Marlin versions:
 2.0.6.1

Options:
 clean => re-download fresh marlin, then setup
 noclean => setup without removing intermediate mod build

Will clone original into: clones/Marlin-<ver>
Will create mod at: mods/<machine>-<ver>
```


### ./listmarlinvers

```
List remote Marlin 2 versions (tags)

Usage:
 ./listmarlinvers
```
