#!/bin/sh
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
cd "${SCRIPTPATH}"
. "./superlib"
. "./default.ini"
. "./config.ini"

# Customizing stock Marlin code

#----------------------------------------------------------

SRC="${1}"
MOD="${2}"
checkdir "${MOD}"
exitonerror ${?}

VERSION="${3}"
if [ "${VERSION}" = "" ]; then
  errorexit "No version"
fi

if [ "${VERSION}" = "2.0.6.1" ]; then
  note "Verified"
else
  warning "Version ${VERSION} not verified, CTRL-C to abort"
  keywait 3
fi

#----------------------------------------------------------

FILE="${MOD}/platformio.ini"
cp "${SRC}/platformio.ini" "${FILE}"

replaceinfile 'default_envs = mega2560' "default_envs = ${PLATFORMIO}" "${FILE}"

if [ "${SDCADRDMOUNT}" != "" ]; then
  UPLOAD="$(printf "[env:${PLATFORMIO}]\nupload_port = ${SDCADRDMOUNT}")"
  replaceinfile "[env:${PLATFORMIO}]" "${UPLOAD}" "${FILE}"
fi

#----------------------------------------------------------

FILE="${MOD}/Marlin/Configuration.h"
cp "${SRC}/Marlin/Configuration.h" "${FILE}"

# author
replaceinfile '(none, default config)' "(${AUTHOR}, ${MACHINE})" "${FILE}"

# baud
replaceinfile 'BAUDRATE 250000' "BAUDRATE ${BAUDRATE}" "${FILE}"

# serial
if [ "${SERIAL_PORT}" != "" ]; then
  replaceinfile 'SERIAL_PORT 0' "SERIAL_PORT ${SERIAL_PORT}" "${FILE}"
fi

# serial 2
if [ "${SERIAL_PORT_2}" != "" ]; then
  replaceinfile 'SERIAL_PORT_2 -1' "SERIAL_PORT_2 ${SERIAL_PORT_2}" "${FILE}"
fi

# motherboard
replaceinfile 'MOTHERBOARD BOARD_RAMPS_14_EFB' "MOTHERBOARD ${MOTHERBOARD}" "${FILE}"

# name/uuid
replaceinfile '//#define CUSTOM_MACHINE_NAME "3D Printer"' "#define CUSTOM_MACHINE_NAME \"${MACHINE}\"" "${FILE}"
replaceinfile '//#define MACHINE_UUID "00000000-0000-0000-0000-000000000000"' "#define MACHINE_UUID \"${UUID}\"" "${FILE}"

# dummy temp 0 = 100 [C]
replaceinfile 'TEMP_SENSOR_0 1' "TEMP_SENSOR_0 999" "${FILE}"

# disable extrusion checks
replaceinfile '#define PREVENT_COLD_EXTRUSION' "//#define PREVENT_COLD_EXTRUSION" "${FILE}"
replaceinfile '#define PREVENT_LENGTHY_EXTRUDE' "//#define PREVENT_LENGTHY_EXTRUDE" "${FILE}"

# enable all end stops
replaceinfile '//#define USE_XMAX_PLUG' "#define USE_XMAX_PLUG" "${FILE}"
replaceinfile '//#define USE_YMAX_PLUG' "#define USE_YMAX_PLUG" "${FILE}"
replaceinfile '//#define USE_ZMAX_PLUG' "#define USE_ZMAX_PLUG" "${FILE}"

# reverse all endstops
replaceinfile 'ENDSTOP_INVERTING false' "ENDSTOP_INVERTING true" "${FILE}"

# enable axes/drivers
replaceinfile '//#define X_DRIVER_TYPE  A4988' "#define X_DRIVER_TYPE ${DRIVER}" "${FILE}"
replaceinfile '//#define Y_DRIVER_TYPE  A4988' "#define Y_DRIVER_TYPE ${DRIVER}" "${FILE}"
replaceinfile '//#define Z_DRIVER_TYPE  A4988' "#define Z_DRIVER_TYPE ${DRIVER}" "${FILE}"
replaceinfile '//#define E0_DRIVER_TYPE A4988' "#define E0_DRIVER_TYPE ${DRIVER}" "${FILE}"

# steps and acceleration
replaceinfile 'DEFAULT_AXIS_STEPS_PER_UNIT   { 80, 80, 4000, 500 }' "DEFAULT_AXIS_STEPS_PER_UNIT ${STEPS_PER_UNIT}" "${FILE}"
replaceinfile 'DEFAULT_MAX_FEEDRATE          { 300, 300, 5, 25 }' "DEFAULT_MAX_FEEDRATE ${MAX_FEEDRATE}" "${FILE}"
replaceinfile 'DEFAULT_MAX_ACCELERATION      { 3000, 3000, 100, 10000 }' "DEFAULT_MAX_ACCELERATION ${MAX_ACCELERATION}" "${FILE}"
replaceinfile 'DEFAULT_ACCELERATION          3000' "DEFAULT_ACCELERATION ${DEFAULT_ACCELERATION}" "${FILE}"
replaceinfile 'DEFAULT_RETRACT_ACCELERATION  3000' "DEFAULT_RETRACT_ACCELERATION ${DEFAULT_ACCELERATION}" "${FILE}"
replaceinfile 'DEFAULT_TRAVEL_ACCELERATION   3000' "DEFAULT_TRAVEL_ACCELERATION ${DEFAULT_ACCELERATION}" "${FILE}"

# jerk and junction deviation
replaceinfile '#define DEFAULT_XJERK 10.0' "#define DEFAULT_XJERK ${DEFAULT_JERK}" "${FILE}"
replaceinfile '#define DEFAULT_YJERK 10.0' "#define DEFAULT_YJERK ${DEFAULT_JERK}" "${FILE}"
replaceinfile '#define DEFAULT_ZJERK  0.3' "#define DEFAULT_ZJERK ${DEFAULT_JERK}" "${FILE}"
replaceinfile '#define DEFAULT_EJERK    5.0' "#define DEFAULT_EJERK ${DEFAULT_JERK}" "${FILE}"
replaceinfile 'JUNCTION_DEVIATION_MM 0.013' "JUNCTION_DEVIATION_MM ${JUNCTION_DEVIATION}" "${FILE}"

# use S curve acceleration
replaceinfile '//#define S_CURVE_ACCELERATION' "#define S_CURVE_ACCELERATION" "${FILE}"

# mm/min between probes
replaceinfile 'XY_PROBE_SPEED (133*60)' "XY_PROBE_SPEED (${XY_PROBE_SPEED})" "${FILE}"

# axes inversion
replaceinfile 'INVERT_X_DIR false' "INVERT_X_DIR ${INVERT_X_DIR}" "${FILE}"
replaceinfile 'INVERT_Y_DIR true' "INVERT_Y_DIR ${INVERT_Y_DIR}" "${FILE}"
replaceinfile 'INVERT_Z_DIR false' "INVERT_Z_DIR ${INVERT_Z_DIR}" "${FILE}"
replaceinfile 'INVERT_E0_DIR false' "INVERT_E0_DIR ${INVERT_E0_DIR}" "${FILE}"
replaceinfile 'INVERT_E1_DIR false' "INVERT_E1_DIR ${INVERT_E1_DIR}" "${FILE}"

# invert z homing
replaceinfile 'Z_HOME_DIR -1' "Z_HOME_DIR 1" "${FILE}"

# travel
replaceinfile 'X_BED_SIZE 200' "X_BED_SIZE ${TRAVELX}" "${FILE}"
replaceinfile 'Y_BED_SIZE 200' "Y_BED_SIZE ${TRAVELY}" "${FILE}"
#replaceinfile '#define X_MIN_POS 0' "#define X_MIN_POS -${TRAVELX}" "${FILE}"
#replaceinfile '#define Y_MIN_POS 0' "#define Y_MIN_POS -${TRAVELY}" "${FILE}"
#replaceinfile '#define Z_MIN_POS 0' "#define Z_MIN_POS -${TRAVELZ}" "${FILE}"
replaceinfile '#define Z_MAX_POS 200' "#define Z_MAX_POS ${TRAVELZ}" "${FILE}"

# homing
replaceinfile 'HOMING_FEEDRATE_XY (50*60)' "HOMING_FEEDRATE_XY (${HOMING_FEEDRATE_XY})" "${FILE}"
replaceinfile 'HOMING_FEEDRATE_Z  (4*60)' "HOMING_FEEDRATE_Z (${HOMING_FEEDRATE_Z})" "${FILE}"

# enable SD-card
replaceinfile '//#define SDSUPPORT' "#define SDSUPPORT" "${FILE}"

replaceinfile '//#define INDIVIDUAL_AXIS_HOMING_MENU' "#define INDIVIDUAL_AXIS_HOMING_MENU" "${FILE}"

#----------------------------------------------------------

FILE="${MOD}/Marlin/Configuration_adv.h"
cp "${SRC}/Marlin/Configuration_adv.h" "${FILE}"

# endstops ON by default!
replaceinfile '//#define ENDSTOPS_ALWAYS_ON_DEFAULT' "#define ENDSTOPS_ALWAYS_ON_DEFAULT" "${FILE}"

# diagonal homing
replaceinfile '//#define QUICK_HOME' "#define QUICK_HOME" "${FILE}"

replaceinfile 'HOMING_BUMP_MM      { 5, 5, 2 }' "HOMING_BUMP_MM ${HOMING_BUMP}" "${FILE}"
replaceinfile 'HOMING_BUMP_DIVISOR { 2, 2, 4 }' "HOMING_BUMP_DIVISOR ${HOMING_BUMP_DIVISOR}" "${FILE}"
replaceinfile '//#define HOMING_BACKOFF_POST_MM { 2, 2, 2 }' "#define HOMING_BACKOFF_POST_MM ${HOMING_BACKOFF_POST}" "${FILE}"

replaceinfile 'DISABLE_INACTIVE_X true' "DISABLE_INACTIVE_X false" "${FILE}"
replaceinfile 'DISABLE_INACTIVE_Y true' "DISABLE_INACTIVE_Y false" "${FILE}"
replaceinfile 'DISABLE_INACTIVE_Z true' "DISABLE_INACTIVE_Z false" "${FILE}"
replaceinfile 'DISABLE_INACTIVE_E true' "DISABLE_INACTIVE_E false" "${FILE}"

replaceinfile '//#define ADAPTIVE_STEP_SMOOTHING' "#define ADAPTIVE_STEP_SMOOTHING" "${FILE}"

replaceinfile 'BLOCK_BUFFER_SIZE 8' "BLOCK_BUFFER_SIZE ${COMMAND_QUEUE}" "${FILE}"
replaceinfile 'BLOCK_BUFFER_SIZE 16' "BLOCK_BUFFER_SIZE ${COMMAND_QUEUE}" "${FILE}"

# ascii in
replaceinfile '#define BUFSIZE 4' "#define BUFSIZE 32" "${FILE}"

# serial out
replaceinfile '#define TX_BUFFER_SIZE 0' "#define TX_BUFFER_SIZE 32" "${FILE}"

# quick stop (M108, M112, M410, M876)
replaceinfile '//#define EMERGENCY_PARSER' "//#define EMERGENCY_PARSER" "${FILE}"

if [ "${TRINAMIC}" = true ]; then

replaceinfile 'HOLD_MULTIPLIER    0.5' "HOLD_MULTIPLIER ${HOLD_MULTIPLIER}" "${FILE}"

replaceinfile '_MICROSTEPS     16' "_MICROSTEPS ${MICROSTEPS}" "${FILE}"
replaceinfile '0_MICROSTEPS    16' "0_MICROSTEPS ${MICROSTEPS}" "${FILE}"
replaceinfile '1_MICROSTEPS    16' "1_MICROSTEPS ${MICROSTEPS}" "${FILE}"
replaceinfile '2_MICROSTEPS    16' "2_MICROSTEPS ${MICROSTEPS}" "${FILE}"

replaceinfile '_RSENSE          0.11' "_RSENSE ${RSENSE}" "${FILE}"
replaceinfile '0_RSENSE         0.11' "0_RSENSE ${RSENSE}" "${FILE}"
replaceinfile '1_RSENSE         0.11' "1_RSENSE ${RSENSE}" "${FILE}"
replaceinfile '2_RSENSE         0.11' "2_RSENSE ${RSENSE}" "${FILE}"

replaceinfile '_MAX_CURRENT     1000' "_MAX_CURRENT ${MAX_CURRENT}" "${FILE}"
replaceinfile '0_MAX_CURRENT    1000' "0_MAX_CURRENT ${MAX_CURRENT}" "${FILE}"
replaceinfile '1_MAX_CURRENT    1000' "1_MAX_CURRENT ${MAX_CURRENT}" "${FILE}"
replaceinfile '2_MAX_CURRENT    1000' "2_MAX_CURRENT ${MAX_CURRENT}" "${FILE}"

replaceinfile '_CURRENT       800' "_CURRENT ${CURRENT}" "${FILE}"
replaceinfile '0_CURRENT      800' "0_CURRENT ${CURRENT}" "${FILE}"
replaceinfile '1_CURRENT      800' "1_CURRENT ${CURRENT}" "${FILE}"
replaceinfile '2_CURRENT      800' "2_CURRENT ${CURRENT}" "${FILE}"

replaceinfile 'X_CURRENT_HOME  X_CURRENT' "X_CURRENT_HOME ${CURRENT_HOME}" "${FILE}"
replaceinfile 'X2_CURRENT_HOME X2_CURRENT' "X2_CURRENT_HOME ${CURRENT_HOME}" "${FILE}"
replaceinfile 'Y_CURRENT_HOME  Y_CURRENT' "Y_CURRENT_HOME ${CURRENT_HOME}" "${FILE}"
replaceinfile 'Y2_CURRENT_HOME Y2_CURRENT' "Y2_CURRENT_HOME ${CURRENT_HOME}" "${FILE}"
replaceinfile 'Z_CURRENT_HOME  Z_CURRENT' "Z_CURRENT_HOME ${CURRENT_HOME}" "${FILE}"
replaceinfile 'Z2_CURRENT_HOME Z2_CURRENT' "Z2_CURRENT_HOME ${CURRENT_HOME}" "${FILE}"

# no stealth, use normal operation
replaceinfile '#define STEALTHCHOP_' "//#define STEALTHCHOP_" "${FILE}"

# use SPI
replaceinfile '//#define TMC_USE_SW_SPI' "#define TMC_USE_SW_SPI" "${FILE}"

# spreadCycle
replaceinfile 'CHOPPER_TIMING CHOPPER_DEFAULT_12V' "CHOPPER_TIMING CHOPPER_DEFAULT_${CHOPPER_DEFAULT}" "${FILE}"

replaceinfile '//#define MONITOR_DRIVER_STATUS' "#define MONITOR_DRIVER_STATUS" "${FILE}"
replaceinfile '//#define SQUARE_WAVE_STEPPING' "#define SQUARE_WAVE_STEPPING" "${FILE}"
replaceinfile '//#define TMC_DEBUG' "#define TMC_DEBUG" "${FILE}"

# enable sensorless homing (will activate both min AND max enstops?)
replaceinfile '//#define SENSORLESS_HOMING' "#define SENSORLESS_HOMING" "${FILE}"
replaceinfile '#define X_STALL_SENSITIVITY  8' "#define X_STALL_SENSITIVITY ${STALL_SENSITIVITY}" "${FILE}"
replaceinfile '#define Y_STALL_SENSITIVITY  8' "#define Y_STALL_SENSITIVITY ${STALL_SENSITIVITY}" "${FILE}"
replaceinfile '//#define Z_STALL_SENSITIVITY  8' "#define Z_STALL_SENSITIVITY ${STALL_SENSITIVITY}" "${FILE}"
replaceinfile '//#define Z2_STALL_SENSITIVITY' "#define Z2_STALL_SENSITIVITY" "${FILE}"

fi

replaceinfile '//#define SPINDLE_FEATURE' "#define SPINDLE_FEATURE" "${FILE}"
replaceinfile '#define CUTTER_POWER_UNIT PWM255' "#define CUTTER_POWER_UNIT ${TOOL_UNIT}" "${FILE}"
replaceinfile 'SPEED_POWER_MIN            5000' "SPEED_POWER_MIN ${TOOL_MIN}" "${FILE}"
replaceinfile 'SPEED_POWER_STARTUP       25000' "SPEED_POWER_STARTUP ${TOOL_STARTUP}" "${FILE}"
replaceinfile 'SPEED_POWER_MAX           30000' "SPEED_POWER_MAX ${TOOL_MAX}" "${FILE}"

# enable M114 R to poll real-time position
replaceinfile '//#define M114_REALTIME' "#define M114_REALTIME" "${FILE}"

replaceinfile '//#define G0_FEEDRATE 3000' "#define G0_FEEDRATE ${G0_FEEDRATE}" "${FILE}"

replaceinfile '//#define PINS_DEBUGGING' "#define PINS_DEBUGGING" "${FILE}"

#----------------------------------------------------------
